<?php

declare(strict_types=1);

namespace Drupal\verification;

use Drupal\Core\Session\AccountInterface;
use Drupal\verification\Result\VerificationResult;
use Symfony\Component\HttpFoundation\Request;

/**
 * Interface for a request verifier.
 */
interface RequestVerifierInterface {

  /**
   * Checks if request is verified for given operation and account.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $operation
   *   The operation to verify.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to verify against.
   * @param string|null $email
   *   (optional) Email address to use.
   *
   * @return \Drupal\verification\Result\VerificationResult
   *   The verification result.
   *
   * @see Drupal\verification\Provider\VerificationProviderInterface
   */
  public function verifyLogin(Request $request, string $operation, AccountInterface $account, ?string $email = NULL): VerificationResult;

  /**
   * Checks if request is verified for given operation and account.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request object.
   * @param string $operation
   *   The operation to verify.
   * @param \Drupal\Core\Session\AccountInterface $account
   *   The user account to verify against.
   * @param string|null $email
   *   (optional) Email address to use.
   *
   * @return \Drupal\verification\Result\VerificationResult
   *   The verification result.
   *
   * @see Drupal\verification\Provider\VerificationProviderInterface
   */
  public function verifyOperation(Request $request, string $operation, AccountInterface $account, ?string $email = NULL): VerificationResult;

}
