<?php

declare(strict_types=1);

namespace Drupal\verification;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\verification\Provider\VerificationProviderInterface;
use Drupal\verification\Result\VerificationResult;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Verifies requests.
 */
class RequestVerifier implements RequestVerifierInterface {

  /**
   * Constructs a RequestVerifier.
   */
  public function __construct(
    protected readonly ClassResolverInterface $classResolver,
    protected readonly LoggerInterface $logger,
    protected readonly array $providerServiceIds,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function verifyLogin(Request $request, string $operation, AccountInterface $account, ?string $email = NULL): VerificationResult {
    $providers = $this->getProviders();
    $finalResult = VerificationResult::unhandled();

    foreach ($providers as $provider) {
      $result = $provider->verifyLogin($request, $operation, $account, $email);

      if ($result->ok) {
        return $result;
      }

      if ($result->err) {
        $finalResult = $result;
      }
    }

    return $finalResult;
  }

  /**
   * {@inheritdoc}
   */
  public function verifyOperation(Request $request, string $operation, AccountInterface $account, ?string $email = NULL): VerificationResult {
    $providers = $this->getProviders();
    $finalResult = VerificationResult::unhandled();

    foreach ($providers as $provider) {
      $result = $provider->verifyOperation($request, $operation, $account, $email);

      if ($result->ok) {
        return $result;
      }

      if ($result->err) {
        $finalResult = $result;
      }
    }

    return $finalResult;
  }

  /**
   * Lazily load providers by service ids.
   *
   * Returns an iterator over provider instances
   * sorted by priority.
   */
  protected function getProviders() {
    foreach ($this->providerServiceIds as $serviceId) {
      $provider = $this->classResolver->getInstanceFromDefinition($serviceId);

      if ($provider instanceof VerificationProviderInterface) {
        yield $provider;
      }
      // Log invalid provider class.
      else {
        $this->logger->warning('Tagged verification provider does not implement VerificationProviderInterface: @serviceId (class: @class)', [
          '@serviceId' => $serviceId,
          '@class' => get_class($provider),
        ]);
      }
    }
  }

}
